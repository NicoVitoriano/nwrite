/*
 * nwrite: write replacement. 
 *
 * Version 1.9.2 - February 9, 1998
 *
 ****************************************************************************
 * The nwrite software package and all files contained therein are covered
 * by this copyright notice & license:
 * 
 * Copyright (c) 1993 Marco Nicosia <marco@csua.berkeley.edu>
 * Copyright (c) 1994-1995 Aaron C. Smith <aaron@csua.berkeley.edu>
 * Copyright (c) 1995-1998 Alan Coopersmith <alanc@csua.berkeley.edu>
 * 
 * Permission to use, copy, modify, and distribute this software in source
 * and/or binary forms and its documentation, without fee and without a 
 * signed licensing agreement, is hereby granted, provided that the 
 * following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. The names of the authors may not be used to endorse or promote
 *     products derived from this software without specific prior written
 *     permission.
 * 
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
 * SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE
 * AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 * 
 * THE AUTHORS SPECIFICALLY DISCLAIM ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY,
 * PROVIDED HEREUNDER IS PROVIDED "AS IS". THE AUTHORS HAVE NO OBLIGATION TO
 * PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS. 
 * 
 ****************************************************************************
 * 
 * system-specific configurations for nwrite
 *
 * To add new definitions, edit acconfig.h and then run autoheader to 
 * generate a new config.h.in, then run configure to generate config.h
 * for your specific machine.
 *
 */
#ifndef NWRITE_CONFIG_H_INCLUDED
#define NWRITE_CONFIG_H_INCLUDED
@TOP@
/*
 * acconfig.h
 *
 *  Descriptive text for the C preprocessor macros that configure scripts 
 *  can define; autoheader copies the ones that configure.in uses into the
 *  configuration header file templates.
 *
 *  The section above the TOP marker is copied verbatim into the generated
 *  config.h.in file.
 *
 *  The entries are in sort -df order: alphabetical, case insensitive,
 *  ignoring punctuation (such as underscores).  Although this order
 *  can split up related entries, it makes it easier to check whether
 *  a given entry is in the file.
 *
 *  Leave the following blank line there!!  Autoheader needs it.  
 */


/* Define if you want users to be able to clear their argv[] */
#undef CLEAR_PROCS

/* Define as TRUE if your system allows any user to write to a mesg y tty */
#undef TTYS_WORLD_WRITABLE

/* Set to the prefix applied to tty file names (normally "tty" or "pts/") */
#undef TTY_PREFIX


/* Leave that blank line there!!  Autoheader needs it.
   If you're adding to this file, keep in mind:
   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  

   Everything after the BOTTOM tag below is copied to the end of the generated
   file
*/
@BOTTOM@
#endif /* NWRITE_CONFIG_H_INCLUDED */
